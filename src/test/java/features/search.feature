Feature: add an item from top 5 brands of a category

As a logged in member I want o search for top 5 brands of any category. I want to add an item from my search to the shopping cart and proceed to the payment page

Scenario: logged in member add an item to a cart
Given logged in member is on landing page
When user navigates to a category
And filter top five brands
And add one item to the shopping cart
Then The item should exist on card
