package au.com.amazon;

import junit.framework.Assert;
import pageObject.LandingPage;
import pageObject.ProductPage;
import resources.Base;

public class Validate extends Base {

	public void validateCartNumber() {
		LandingPage landingP = new LandingPage();
		Assert.assertEquals(landingP.getNumberItemOnCart().getText(), "1");
	}

	public void validateMenuTitle() {
		LandingPage landingP = new LandingPage();
		Assert.assertEquals(landingP.getMenuTitle().getText(), "SHOP BY CATEGORY");
	}

	public void validatePageTitle(String title) {
		ProductPage productP = new ProductPage();
		Assert.assertEquals(productP.getH1().getText(), title);
	}

}
