package au.com.amazon;
import pageObject.LandingPage;
import pageObject.LoginPage;
import resources.Base;

public class Login extends Base {
	
	public void getMemberLogin(String email, String password) {
		LandingPage landingP = new LandingPage();
	    landingP.getAccount().click();
	    LoginPage loginP = new LoginPage();
	    System.out.println(email);
	    loginP.getEmail().sendKeys(email);
	    loginP.getPassword().sendKeys(password);
	    loginP.getLogin().click();

}	
}
