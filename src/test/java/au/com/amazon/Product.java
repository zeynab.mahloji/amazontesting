package au.com.amazon;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

import pageObject.ProductPage;
import resources.Base;

public class Product extends Base {
	ProductPage product = new ProductPage();

	public void clickOnFiveTopBrands() {
		for (int i = 3; i <= 6; i++) {
			WebElement checkbox = product.getCheckBox(i);
			checkbox.click();
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

		}
	}

	public void ClickOnSize() {
		product.getSizeDrp().click();
		product.getSize();

	}

}
