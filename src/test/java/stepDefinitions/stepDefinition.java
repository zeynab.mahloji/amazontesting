package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.junit.Cucumber;
import pageObject.ProductPage;
import resources.Base;
import au.com.amazon.Login;
import au.com.amazon.Product;
import au.com.amazon.Validate;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
public class stepDefinition extends Base {

	Login login = new Login();
	Validate validate = new Validate();
	ProductPage productP = new ProductPage();
	Product product = new Product();
	
	@Given("^logged in member is on landing page$")
	public void logged_in_member_is_on_landing_page() throws Throwable {
		initializeDriver();
		navigateTo("baseUrl");
		
        String email = getPropertyData("email");
		String password = getPropertyData("password");
	
		login.getMemberLogin(email,password);
	}

	@When("^user navigates to a category$")
	public void navigate_to_category() throws Throwable {
		navigateTo("category");
        validate.validatePageTitle("Women's Pants");

	}
	@And("^filter top five brands$")
	public void filter_top_five_brands() throws Throwable {
		productP.getFirstBrand().click();
		product.clickOnFiveTopBrands();
		
	}
	@And("^add one item to the shopping cart$")
	public void add_one_item_to_the_shopping_cart() throws Throwable {
		
		productP.getFirstItem().click();
		product.ClickOnSize();
		Thread.sleep(3000);
		productP.addToCart().click();

	}

	@Then("^The item should exist on card$")
	public void the_item_should_exist_on_card() throws Throwable {
		validate.validateCartNumber();
//		driver.quit();
	}

		
}
