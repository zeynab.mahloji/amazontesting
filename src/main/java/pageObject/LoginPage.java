package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import resources.Base;

public class LoginPage extends Base {

	By email = By.xpath("//input[@name='email']");
	By password = By.xpath("//input[@name='password']");
	By signIn = By.xpath("//input[@class='a-button-input']");

	public WebElement getEmail() {
		return driver.findElement(email);
	}

	public WebElement getPassword() {
		return driver.findElement(password);
	}

	public WebElement getLogin() {
		return driver.findElement(signIn);
	}

}
