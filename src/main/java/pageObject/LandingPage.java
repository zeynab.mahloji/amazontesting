package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import resources.Base;

public class LandingPage extends Base {

	By account = By.xpath("//a[@id='nav-link-accountList']");
	By NavBar = By.xpath("//a[@id='nav-hamburger-menu']");
	By category = By.xpath("//div[contains(text(),'Women's Clothing & Accessories')]");
	By subCategory = By.xpath("//*[@id=\"hmenu-content\"]/ul[15]/li[7]/a/div");
	By signInButton = By.xpath("//a[@data-nav-ref='nav_signin']");
	By cartNumber = By.xpath("//span[@id='nav-cart-count']");
	By menuTitle = By.className("hmenu-title");


	public WebElement getMenuTitle() {
		return driver.findElement(menuTitle);
	}

	public WebElement getAccount() {
		return driver.findElement(account);
	}

	public WebElement ClickOnSignInBtn() {
		return driver.findElement(signInButton);
	}

	public WebElement getNavigationBar() {

		return driver.findElement(NavBar);
	}

	public WebElement getCategory() {
		return driver.findElement(category);
	}

	public WebElement getSubCategory() {
		return driver.findElement(subCategory);
	}

	public WebElement getNumberItemOnCart() {
		return driver.findElement(cartNumber);
	}
}
