package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import resources.Base;

public class ProductPage extends Base {
	

	By firstBrand = By.xpath("//h4[text()='Featured Brands']/following::ul[1]/div/li[1]");
	By firstItem = By.xpath("//div[@class='s-result-list s-search-results sg-row']/div[1]/div[1]/div/div/div[2]/div");
	By sizeDropDown = By.xpath("//select[@id='native_dropdown_selected_size_name']");
	By addToCartBtn = By.xpath("//input[@id='add-to-cart-button']");
	By h1 = By.xpath("//h1");


	public WebElement getFirstBrand() {
		return driver.findElement(firstBrand);
	}

	public WebElement getFirstItem() {
		return driver.findElement(firstItem);
	}

	public WebElement getSizeDrp() {
		return driver.findElement(sizeDropDown);
	}

	public WebElement addToCart() {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(addToCartBtn));
		return element;

	}

	public Select getSize() {
		Select dropSize = new Select(
		driver.findElement(By.xpath("//select[@id='native_dropdown_selected_size_name']")));
	    dropSize.selectByIndex(1);
		return dropSize;
	}

	public WebElement getCheckBox(int i) {
		WebElement checkBox = driver.findElement(By.xpath("//*[text()='Featured Brands']/following::ul[1]/li[" + i + "]//a"));
		return checkBox;
	}

	public WebElement getH1() {
		return driver.findElement(h1);
	}
}
