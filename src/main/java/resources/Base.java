package resources;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Coordinates;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;

public class Base {

	public static WebDriver driver;
	public Properties properties;

	public WebDriver initializeDriver() throws IOException {
		properties = new Properties();
	    Path path = Paths.get("src", "main", "java", "resources", "data.properties");
		FileInputStream file = new FileInputStream(path.toString());
		properties.load(file);
		String browserName = properties.getProperty("browser");
		if (browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", properties.getProperty("chromeDriverPath"));
			driver = new ChromeDriver();
		} else if (browserName.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", properties.getProperty("firefoxDriverPath"));
			driver = new FirefoxDriver();
		}

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}

	public WebDriver navigateTo(String key) {
		String urlAddress = properties.getProperty(key);
		driver.get(urlAddress);
		return driver;
	}

	public String getPropertyData(String key) {
		return properties.getProperty(key);
	}
	
//	@After
//	public void closebrowser() {
//	driver.quit();
//	}


	public static void main(String[] args) {

	}

}
