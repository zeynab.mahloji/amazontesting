#Amazon Testing
This code is E2E test for amazon.com.au

##Configuration
place the path of firefox and chrome web driver in your machine here:

``\src\main\java\resources\data.properties``

set the browser to firefox or chrome

##Run the tests
``mvn test``

Note: the ChromeWebDriver version should be compatible with your Chrome version installed on your machine